import scrapy
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from supercoloring_scraper.items import SupercoloringScraperItem
from urllib.parse import urljoin, urlparse


class SupercoloringSpider(CrawlSpider):
    name = "supercoloring"
    allowed_domains = ["supercoloring.com"]
    start_urls = ["https://www.supercoloring.com/sections/coloring-pages"]

    rules = (
        Rule(
            LinkExtractor(
                allow=("https://www.supercoloring.com/coloring-pages/",),
                unique=True,
            ),
            callback="parse_page",
            follow=True,
        ),
    )

    def parse_page(self, response):
        if response.url.endswith("=online") or response.url.endswith("=print"):
            return

        src = response.css("img::attr(src)").get()

        if src and urlparse(src).path.startswith(
            "/sites/default/files/styles/coloring_full/public"
        ):
            extra_fields = response.css(".extra-fields *::text").getall()

            extra_field_dict = {}
            current_key = ""
            for i, _ in enumerate(extra_fields):
                if ":\xa0" in _:
                    current_key = _.replace(":\xa0", "")
                    extra_field_dict[current_key] = ""
                    continue
                if current_key:
                    extra_field_dict[current_key] += _

            for key in extra_field_dict:
                extra_field_dict[key] = extra_field_dict[key].replace("\n", "")

            item = SupercoloringScraperItem()
            item["src"] = src.replace("coloring_full", "coloring_medium")
            item["referer"] = response.url
            item["extra_fields"] = extra_field_dict
            yield item
