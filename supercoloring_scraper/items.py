# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class SupercoloringScraperItem(scrapy.Item):
    referer = scrapy.Field()
    src = scrapy.Field()
    extra_fields = scrapy.Field()
