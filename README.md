# Images

supercoloring.com is a great source for images. We are mainly interested in coloring pages from this site. For each image they indicate the license, so we will capture that information as well.

you are pretty much running the spider, main.py, update_algolia_index.py, then adding the images/supercoloring & images/supercoloring/clean.

```
    - python -m venv .venv
    - source .venv/bin/activate
    - pip install Scrapy
    - rm supercoloring.jsonl
    - scrapy crawl supercoloring -o supercoloring.jsonl

    - python -m venv .venv
    - source .venv/bin/activate
    - pip install Pillow
    - python main.py

    - python -m venv .venv
    - source .venv/bin/activate
    - pip install algoliasearch
    - python update_algolia_index.py

    - git add images/supercoloring
    - git add images/supercoloring/clean
    - git commit -m "..."
    - git push origin main
```
