import os


def get_folder_size(folder_path):
    total_size = 0
    for dirpath, _, filenames in os.walk(folder_path):
        for filename in filenames:
            file_path = os.path.join(dirpath, filename)
            total_size += os.path.getsize(file_path)
    return total_size


folder_path = "./images/supercoloring/clean"
total_size_bytes = get_folder_size(folder_path)


def convert_bytes(size):
    for x in ["bytes", "KB", "MB", "GB", "TB"]:
        if size < 1024.0:
            return "%3.1f %s" % (size, x)
        size /= 1024.0


total_size_readable = convert_bytes(total_size_bytes)

print(f"Total size of images: {total_size_readable}")
