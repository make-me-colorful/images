from concurrent.futures import ThreadPoolExecutor
import json
import os
import random
import re
import time

from PIL import Image
import requests


def read_jsonl_data(filename):
    data = []
    with open(filename, "rt") as f:
        for line in f.readlines():
            line = json.loads(line)
            data.append(line)
    return data


def write_jsonl_data(filename, data, mode="wt"):
    if isinstance(data, dict):
        data = [data]
    with open(filename, mode) as f:
        for d in data:
            line = json.dumps(d)
            f.write(line + "\n")


def licensable(line):
    if not line["referer"].startswith("https://www.supercoloring.com/coloring-pages/"):
        return  # we do this as to not duplicate images (from other languages)

    inappropriate = False
    for key in line["extra_fields"]:
        for inappropriate_word in [
            "inappropriate",
            "drug",
            "weed",
            "marijuana",
            "dirty",
        ]:
            if inappropriate_word in line["extra_fields"][key].lower():
                inappropriate = True
                break
        if inappropriate:
            break

    if inappropriate:
        return

    permission = line["extra_fields"].get("Permission")
    if not permission:
        return
    if "non-commercial" in permission.lower():
        return
    if "commercial" in permission.lower():
        return line


def get_filename_from_url(url):
    m = re.search(r"(\d{4})/(\d{2})/([^/]+\.[^.]+$)", url)
    try:
        parts = m.groups()
        return "-".join(parts)
    except AttributeError:
        return url.split("/")[-1]


def get_filename_keywords(filename_):
    keywords = re.findall(r"\b([A-z]+)\b", filename_.split("/")[-1].split(".")[0])
    if not keywords:
        raise Exception(filename_)

    return [_ for _ in keywords if len(_) > 2 and keywords not in ("coloring", "page")]


def download_image(session, line, jsonl_log, destination_folder):

    filename = get_filename_from_url(line["src"])
    if os.path.exists(os.path.join("images", destination_folder, filename)):
        return

    time.sleep(random.random())
    r = session.get(line["src"])
    if r.status_code == 200:
        print(f"fetched: {destination_folder}/{filename}")

        line_.update({"objectID": f"{destination_folder}/{filename}"})
        write_jsonl_data(jsonl_log, line_, "a")

        with open(os.path.join("images", destination_folder, filename), "wb") as f:
            f.write(r.content)


def add_algolia_attributes(line):
    line["extra_fields"].update(
        {"filename-keywords": get_filename_keywords(line["src"])}
    )


def is_color(pixel):
    min_value = min(pixel[:3])
    max_value = max(pixel[:3])
    difference = max_value - min_value
    threshold_difference = 30
    return difference > threshold_difference


def is_black_or_grey(pixel):
    threshold = 247
    r, g, b = pixel[:3]
    avg_intensity = (r + g + b) // 3
    pixel_is_a_color = is_color(pixel)
    return avg_intensity < threshold and not pixel_is_a_color


def keep_black_or_grey_pixels(image_path):
    img = Image.open(image_path)
    img = img.convert("RGBA")
    pixel_data = img.getdata()
    new_data = []
    for item in pixel_data:
        if not is_black_or_grey(item):
            new_data.append((255, 255, 255, 0))  # Transparent pixel
        else:
            new_data.append(item)
    img.putdata(new_data)
    return img


def process_image(image_info):
    """this process changes extensions to png

    get the objectID from the file.  this may or may not already have been modified.
    if it has been modified,
        then we will see a jpg in images/ and a png in images/clean
    """
    original_extension = image_info["src"].split(".")[-1]  # jpg / png

    destination_folder, object_id_filename = image_info["objectID"].split("/")

    original_image_path = os.path.join(
        "images",
        destination_folder,
        (
            object_id_filename
            if original_extension == "png"
            else object_id_filename.replace(".png", ".jpg")
        ),
    )
    clean_image_path = os.path.join(
        "images", destination_folder, "clean", object_id_filename
    ).replace(".jpg", ".png")

    if os.path.exists(clean_image_path):
        return

    new_image = keep_black_or_grey_pixels(original_image_path)
    new_image = new_image.quantize(
        method=2
    )  # compress image (reduce filesize about 75%)
    new_image.save(clean_image_path, optimize=True)
    print(f"processed: {clean_image_path}")

    # now we need to update the objectID
    if original_extension == "jpg":
        image_info["objectID"] = image_info["objectID"].replace(".jpg", ".png")


def clean_images(data):
    with ThreadPoolExecutor() as executor:
        executor.map(process_image, data)


if __name__ == "__main__":

    data_ = read_jsonl_data("supercoloring.jsonl")
    licensable_data = []
    for line_ in data_:
        l = licensable(line_)
        if l:
            add_algolia_attributes(l)
            licensable_data.append(l)

    session_ = requests.Session()
    session_.headers.update(
        {
            "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36"
        }
    )
    for line_ in licensable_data:
        # TODO: you should probably log after you clean
        download_image(
            session_,
            line_,
            jsonl_log="supercoloring-with-images.jsonl",  # log details of success to this file
            destination_folder="supercoloring",  # download file to this folder images/{destination_filder}
        )

    data_ = read_jsonl_data("supercoloring-with-images.jsonl")
    clean_images(data_)
    # we write again because clean_images changes jpg to png, so our objectID needs to chage
    write_jsonl_data("supercoloring-with-images.jsonl", data_)
