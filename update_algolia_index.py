from main import read_jsonl_data


def update_index(client, index_name, data):
    index = client.init_index(index_name)
    index.set_settings(
        {
            "searchableAttributes": [
                "extra_fields.filename-keywords",
                "extra_fields.Coloring books",
                "extra_fields.Categories",
                "extra_fields.Permission",
            ],
            "removeStopWords": True,
            "queryLanguages": ["en"],
            "indexLanguages": ["en"],
            "ignorePlurals": True,
        }
    )
    index.save_objects(data)


if __name__ == "__main__":
    import os

    from dotenv import load_dotenv
    from algoliasearch.search_client import SearchClient

    load_dotenv()
    algolia_client = client = SearchClient.create(
        os.environ["APP_ID"], os.environ["ADMIN_API_KEY"]
    )
    data = read_jsonl_data("supercoloring-with-images.jsonl")
    update_index(algolia_client, "coloring-pages", data)
